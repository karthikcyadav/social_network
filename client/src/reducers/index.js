import { combineReducers } from "redux";
import authRed from "./authRed";
import errorRed from "./errorRed";
import profileRed from "./profileRed";
import postRed from "./postRed";

export default combineReducers({
  auth: authRed,
  errors: errorRed,
  profile: profileRed,
  post: postRed
});
