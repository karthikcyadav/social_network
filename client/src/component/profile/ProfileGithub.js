import React, { Component } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";

class ProfileGithub extends Component {
  constructor(props) {
    super(props);
    this.state = {
      clientId: "fd3d6b9c99737f866c29",
      clientSecret: "d7b6cfad41028238104e5f613095c95558b522a5",
      count: 5,
      sort: "created: asc",
      repos: []
    };
  }

  componentDidMount() {
    const { username } = this.props;
    const { clientId, clientSecret, count, sort } = this.state;

    fetch(
      `https://api.github.com/users/${username}/repos?per_page=${count}&sort=${sort}&client_id=${clientId}&client_secret=${clientSecret}`
    )
      .then(res => res.json())
      .then(data =>
        this.setState({
          repos: data
        })
      )
      .catch(err => console.log("error", err));
  }

  render() {
    const { repos } = this.state;

    const repoItems = repos.map(item => (
      <div key={item.id} className="card-body mb-2">
        <div className="row">
          <div className="col-md-6">
            <h4>
              <Link
                to={item.html_url}
                className="text-info"
                target="_blank"
                rel="noopener noreferrer"
              >
                {item.name}
              </Link>
            </h4>
            <p>{item.description}</p>
          </div>
          <div className="col-md-6">
            <span className="badge badge-info mr-1">
              Watchers: {item.watchers_count}
            </span>
            <span className="badge badge-success">
              Forks: {item.Forks_count}
            </span>
          </div>
        </div>
      </div>
    ));

    return (
      <div>
        <hr />
        <h3 className="mb-4"> Latest Github Repos</h3>
        {repoItems}
      </div>
    );
  }
}

ProfileGithub.propTypes = {
  username: PropTypes.string.isRequired
};

export default ProfileGithub;
